/*
 * This file is part of pace
 *
 * Copyright 2011 Milian Wolff <mail@milianw.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <QDebug>
#include <QCoreApplication>

#include <signal.h>

#include "runner.h"

Pace::Runner runner;

void signalHandler(int /*sig*/)
{
  static bool handlingSignal = false;

  if (handlingSignal) {
    // never do this more than once
    abort();
  }
  handlingSignal = true;

  // detach signal handler
  signal(SIGINT, SIG_IGN);

  // finalize gracefully
  runner.terminate();

  runner.waitForFinished();

  exit(0);
}

static QTextStream cout(stdout);
static QTextStream cerr(stderr);

void usage()
{
  cout << "Usage: pace-monitor [options] [-p PID | debuggee]" << endl
       << endl
       << "A sampling based application profiler based on GDB." << endl
       << endl
       << "Start application for profiling:" << endl
       << "  pace-monitor [options] your_app [your_args]" << endl
       << endl
       << "Attach to application for profiling:" << endl
       << "  pace-monitor [options] -p PID" << endl
       << endl
       << "Options:" << endl
       << "  -t TIMEOUT\tsampling interval in ms" << endl
       << "            \tset to 0 for variable interval"
       << "            \tbased on the time required for taking a snapshot" << endl
       << "  -h, --help\tshow this help and exit" << endl
       ;
}

void usageError(const QString& error)
{
  cerr << "pace-monitor:" << error << endl
       << "pace-monitor: use --help to get a list of available command line options." << endl;
}

int getPositiveIntOption(QStringList& args, const QString& key1, const QString& key2)
{
  int i = args.indexOf(key1);
  if (i == -1) {
    i = args.indexOf(key2);
  }
  if (i == -1 || i + 1 >= args.size()) {
    return -1;
  }
  bool ok;
  args.takeAt(i);
  int ret = args.takeAt(i).toInt(&ok);
  if (!ok) {
    return -1;
  }
  return ret;
}

int main(int argc, char **argv) {
  QCoreApplication app(argc, argv);

  QStringList args = app.arguments();
  args.takeFirst();

  if (args.contains("-h") || args.contains("--help")) {
    usage();
    return 0;
  }

  if (args.contains("--timeout") || args.contains("-t")) {
    int timeout = getPositiveIntOption(args, "--timeout", "-t");
    if (timeout == -1) {
      usageError("Invalid timeout argument");
      return 1;
    }
    runner.setTimeout(timeout);
  }

  if (args.isEmpty()) {
    usageError("Either debuggee or -p <PID> must be given.");
    return 1;
  }

  if (args.contains("--pid") || args.contains("-p")) {
    int pid = getPositiveIntOption(args, "--pid", "-p");
    if (pid == -1) {
      usageError("Invalid PID argument");
      return 1;
    }
    runner.attachToApp(pid);
  } else {
    if (!runner.startApp(args)) {
      return 1;
    }
  }

  sighandler_t prev_fn = signal(SIGINT, signalHandler);
  if (prev_fn == SIG_IGN) {
    signal(SIGINT, SIG_IGN);
    qWarning() << "could not set signal handler for graceful termination.";
  }

  return app.exec();
}
