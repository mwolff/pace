/*
 * This file is part of pace
 *
 * Copyright 2011 Milian Wolff <mail@milianw.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "runner.h"

#include <QTime>
#include <QTimer>
#include <QTextStream>
#include <QCoreApplication>
#include <QFile>
#include <QDir>
#include <QDebug>

#ifdef HAVE_KDE
#include <KStandardDirs>
#include <KFilterDev>
#endif // HAVE_KDE

#include <signal.h>

// show all GDBMI output
#define IF_DEBUG(x)

// show elapsed time for handling a snapshot
#define IF_TIMER_DEBUG(x)

// uncomment to disable output file compression
// #define DISABLE_COMPRESSION

#ifndef HAVE_KDE
#ifndef DISABLE_COMPRESSION
#define DISABLE_COMPRESSION
#endif
#endif

using namespace Pace;

static QTextStream cout(stdout);
static QTextStream cerr(stderr);

static QString gdbExe()
{
#ifdef HAVE_KDE
  const QString gdb = KStandardDirs::findExe("gdb");
  if (gdb.isEmpty()) {
    qFatal("could not find GDB executable - exiting.");
  }
  return gdb;
#else
  return "gdb";
#endif
}

Runner::Runner(QObject* parent)
: QObject(parent)
, m_timer(new QTimer(this))
, m_gdbProcess(new QProcess(this))
, m_pid(0)
, m_device(0)
, m_cmd(NoCommand)
, m_state(NoState)
, m_hasError(false)
, m_samples(0)
, m_timeout(25)
{
  IF_TIMER_DEBUG(m_elapsed.start();)

  m_timer->setSingleShot(true);
  connect(m_timer, SIGNAL(timeout()), this, SLOT(timeout()));

  m_gdbProcess->setProcessChannelMode(QProcess::SeparateChannels);

  connect(m_gdbProcess, SIGNAL(readyReadStandardError()),
          this, SLOT(readProcessErr()));
  connect(m_gdbProcess, SIGNAL(readyReadStandardOutput()),
          this, SLOT(readProcessOut()));
  connect(m_gdbProcess, SIGNAL(error(QProcess::ProcessError)),
          this, SLOT(processError(QProcess::ProcessError)));
  connect(m_gdbProcess, SIGNAL(finished(int,QProcess::ExitStatus)),
          this, SLOT(processFinished(int,QProcess::ExitStatus)));
}

Runner::~Runner()
{
  if (m_device) {
    delete m_device;
    m_device = 0;
  }
}

void Runner::setTimeout(int timeout)
{
  m_timeout = timeout;
}

void Runner::readProcessErr()
{
  cerr << m_gdbProcess->readAllStandardError() << flush;
}

inline void Runner::handlePrompt()
{
  Q_ASSERT(m_state == Stopped);
  IF_DEBUG(qDebug() << "got prompt, cmd is:" << m_cmd;)

  switch (m_cmd) {
    case NoCommand:
    case Stopping:
      break;
    case StartTimer:
    case Starting:
    case Interrupt:
      Q_ASSERT(false);
      break;
    case Attaching:
      m_cmd = InfoProc;
      sendGDBCmd("info proc");
      break;
    case InfoProc:
      m_cmd = DisablePrettyPrinter;
      sendGDBCmd("disable pretty-printer");
      break;
    case DisablePrettyPrinter:
      cout << "now beginning to sample application" << endl;
      m_elapsed.start();
      m_cmd = GetSample;
      // fallthrough to directly get first backtrace
    case GetSample:
      // get full backtrace
      m_cmd = ReadSample;
      m_out << "sample:\n";
      sendGDBCmd("thread apply all bt");
      break;
    case ReadSample:
      // now continue
      sendGDBCmd("-exec-continue");
      m_cmd = StartTimer;
      break;
    case Exit:
      sendGDBCmd("-gdb-exit");
      // leave m_cmd = Exit!
      break;
  }
}

void Runner::readProcessOut()
{
  if (m_cmd == Stopping) {
    return;
  }

  while(m_gdbProcess->canReadLine()) {
    const QByteArray line = m_gdbProcess->readLine();
    if (line.isEmpty()) {
      continue;
    }

    const char& first = line.at(0);

    if (m_cmd == ReadSample && first == '~') {
      IF_TIMER_DEBUG(cout << m_elapsed.elapsed() << endl;)
      m_out << line;
      continue;
    } else if (m_cmd == Attaching && first == '&' && line.startsWith("&\"ptrace: ")) {
      qWarning() << endl << "Could not attach to process " << m_pid << endl
                 << "Make sure that the process is running, that no other" << endl
                 << "GDB/Pace instance is attached to this process" << endl
                 << "and that your system settings allow attaching." << endl
                 << endl
                 << "GDB Message was: ptrace: Operation not permitted." << endl;
      m_cmd = Exit;
      m_hasError = true;
      continue;
    } else if (m_state == Stopped && first == '(' && line == "(gdb) \n") {
      handlePrompt();
      continue;
    } else if (m_state == NoState && first == '(' && line == "(gdb) \n") {
      // attached to app
      m_state = Stopped;
      handlePrompt();
      continue;
    } else if (m_state == Running && first == '(' && line == "(gdb) \n") {
      // ignore spruse prompt after -exec-continue
      IF_DEBUG(cout << "ignore prompt:" << line;)
      continue;
    } else if (m_cmd == InfoProc && first == '~') {
      IF_DEBUG(cout << "info proc:" << line;)
      m_out << line;
      continue;
    } else if (m_cmd == StartTimer && first == '*' && line.startsWith("*running")) {
      IF_DEBUG(cout << "running/starting:" << line;)
      Q_ASSERT(m_state == Stopped);
      m_state = Running;
      m_cmd = Interrupt;
      IF_TIMER_DEBUG(qDebug() << "snapshot took:" <<  m_elapsed.elapsed() << "ms";)
      ++m_samples;
      cout << "took snapshot #" << m_samples << endl;
      m_timer->start(m_timeout == 0 ? m_elapsed.elapsed() : m_timeout);
      m_elapsed.start();
      continue;
    } else if (m_cmd == Attaching && first == '*' && line.startsWith("*stopped")) {
      IF_DEBUG(cout << "attached:" << line;)
      m_state = Stopped;
      continue;
    } else if (first == '&') {
      // the command we send, ignore it
      IF_DEBUG(cout << "ignore:" << line;)
      continue;
    } else if (first == '=' || first == '~') {
      // TODO: = is info, e.g. thread exited, might want to cover that in the future
      //         also: thread-group i.e. child-process ended
      IF_DEBUG(cout << "ignore:" << line;)
      continue;
    } else if (first == '^' && (line == "^done\n" || line == "^running\n")) {
      // command result
      IF_DEBUG(cout << "ignore cmd:" << line;)
      continue;
    } else if (m_cmd == Interrupt && first == '*' && line.startsWith("*stopped") && line.contains("signal-meaning=\"Interrupt\"")) {
      IF_DEBUG(cout << "interrupted:" << line << flush;)
      // interrupt from our side, we just have to wait for the prompt
      m_state = Stopped;
      m_cmd = GetSample;
      continue;
    } else if (first == '*' && line.startsWith("*stopped")) {
      if (line.endsWith("reason=\"exited-normally\"\n")) {
        IF_DEBUG(cout << "stopped:" << line;);
      } else {
        cerr << "unknown reason for stop - exiting pace:" << line << flush;
      }
      m_state = Stopped;
      m_cmd = Exit;
      continue;
    } else if (m_state == Running && first == '*' && line.startsWith("*running")) {
      // this happens sometimes when additional libs are loaded it seems
      // example: attach to kwrite, open file dialog.
      IF_DEBUG(cout << "ignore running:" << line;)
      continue;
    } else if (m_state == Stopped && m_cmd == Exit && line == "^exit\n") {
      IF_DEBUG(cout << "debuggee exitted" << endl;)
      m_state = Exited;
      continue;
    }

    cerr << "ERROR: unhandled gdb message of length " << line.length() << " in state " << m_state << " and cmd " << m_cmd << endl << line << endl;
    Q_ASSERT(false);
  }
  IF_DEBUG(cout << flush;)
}

void Runner::attachToApp(const qint64 pid)
{
  qDebug() << "attaching pace to debuggee: " << pid;

  Q_ASSERT(pid > 0);
  setPid(pid);

  m_cmd = Attaching;
  m_gdbProcess->start(gdbExe(), QStringList() << "-i" << "mi" << "-p" << QString::number(pid));
}

bool Runner::startApp(const QStringList& debuggee)
{
  qDebug() << "starting debuggee: " << debuggee;
  QProcess proc;
  proc.setProcessChannelMode(QProcess::ForwardedChannels);
  qint64 pid = 0;
  if (proc.startDetached(debuggee.first(), debuggee.mid(1), QDir::currentPath(), &pid)) {
    attachToApp(pid);
    return true;
  } else {
    qWarning() << "failed to start debuggee:" << proc.errorString();
    return false;
  }
}

void Runner::processError(QProcess::ProcessError /*error*/)
{
  qFatal("failed to run pace profiler: %s", qPrintable(m_gdbProcess->errorString()));
}

void Runner::processFinished(int code, QProcess::ExitStatus status)
{
  if (code != 0 || status != QProcess::NormalExit) {
    qDebug() << "process finished:" << code << status << m_gdbProcess->errorString();
  }

  if (m_hasError && m_device) {
    qWarning() << "removing outputfile " << m_fileName;
    QFile::remove(m_fileName);
  } else {
    qDebug() << "finishing profile";
    m_out.flush();
  }

  if (m_device) {
    m_device->close();
  }

  qApp->exit(m_hasError ? 1 : 0);
}

void Runner::sendGDBCmd(const QString& cmd)
{
  Q_ASSERT(m_state == Stopped);
  IF_DEBUG(cout << "SEND:\t" << cmd << endl;)
  m_gdbProcess->write(cmd.toLocal8Bit() + '\n');
}

void Runner::interrupt()
{
  Q_ASSERT(m_cmd == Interrupt);
  Q_ASSERT(m_pid > 0);
  IF_DEBUG(cout << "INTERRUPT" << endl;)
  kill(m_pid, SIGINT);
}

void Runner::timeout()
{
  if (m_state == Exited) {
    return;
  }
  IF_TIMER_DEBUG(qDebug() << "actual elapsed between timeout:" << m_elapsed.elapsed();)
  m_elapsed.start();
  interrupt();
}

void Runner::terminate()
{
  if (m_gdbProcess->state() == QProcess::NotRunning) {
    return;
  }

  m_cmd = Stopping;
  m_gdbProcess->terminate();
  m_timer->stop();
}

void Runner::waitForFinished()
{
  if (m_gdbProcess->state() == QProcess::NotRunning) {
    return;
  }

  m_gdbProcess->waitForFinished();
}

void Runner::setPid(int pid)
{
  Q_ASSERT(pid > 0);
  Q_ASSERT(pid != qApp->applicationPid());
  Q_ASSERT(!m_pid);
  Q_ASSERT(!m_device);

  m_pid = pid;

  QString fileName = QString("pace.out.%1").arg(pid);

#ifndef DISABLE_COMPRESSION
  const QString extension(".gz");
#else
  const QString extension;
#endif

  if (QFile::exists(fileName + extension)) {
    int version = 1;
    QString newName;
    do {
      newName = fileName + '.' + QString::number(version);
      ++version;
    } while (QFile::exists(newName + extension));
    fileName = newName;
  }
  m_fileName = fileName + extension;
  Q_ASSERT(!QFile::exists(m_fileName));

#ifndef DISABLE_COMPRESSION
  m_device = KFilterDev::deviceForFile(m_fileName,
                                       // compressed:
                                       "application/x-gzip", true);
#else // DISABLE_COMPRESSION
  m_device = new QFile(m_fileName);
#endif // DISABLE_COMPRESSION

  Q_ASSERT(m_device);
  m_device->open(QIODevice::WriteOnly);
  Q_ASSERT(m_device->isOpen());
  Q_ASSERT(m_device->isWritable());

  m_out.setDevice(m_device);

  cout << "profiling started, debuggee pid: " << m_pid << ", sampling interval: " << m_timer->interval() << "ms" << endl;

  m_out << "paceprofile" << endl;
  m_out << "interval: " << m_timer->interval() << endl;
}

#include "runner.moc"
