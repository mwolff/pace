/*
 * This file is part of pace
 *
 * Copyright 2011 Milian Wolff <mail@milianw.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#ifndef RUNNER_H
#define RUNNER_H

#include <QObject>
#include <QProcess>
#include <QTextStream>
#include <QTime>
#include <QHash>
#include <QQueue>

class QTimer;

namespace Pace {

/**
 * Starts/attaches to an application using GDB.
 *
 * Sends interrupt to application on timeout
 * and asks for backtrace.
 */
class Runner : public QObject {
  Q_OBJECT

public:
  explicit Runner(QObject* parent = 0);
  virtual ~Runner();

  /// start debuggee and profile it
  bool startApp(const QStringList& debuggee);

  /// attach to debuggee and profile it
  void attachToApp(const qint64 pid);

  /// set timeout intervall in ms after which we query for a backtrace
  void setTimeout(int timeout);

  void waitForFinished();

  /// stop profiling and write profile data to file
  void terminate();

  /// interrupt debugger
  void interrupt();

private slots:
  void timeout();

  void readProcessOut();
  void readProcessErr();

  void processError(QProcess::ProcessError error);
  void processFinished(int code, QProcess::ExitStatus status);

private:
  inline void handlePrompt();
  void sendGDBCmd(const QString& cmd);
  void setPid(int pid);

  // interrupt timer
  QTimer* m_timer;
  // gdb process
  QProcess* m_gdbProcess;
  // pid of debugee
  int m_pid;
  // gzip'ed output device
  // gets set once we know the pid of the debuggee
  QIODevice* m_device;
  // filename for output
  QString m_fileName;
  // text stream to write to m_device
  QTextStream m_out;

  enum Command {
    NoCommand,
    Attaching,
    InfoProc,
    DisablePrettyPrinter,
    GetSample,
    ReadSample,
    StartTimer,
    Starting,
    Stopping,
    Interrupt,
    Exit
  };
  Command m_cmd;
  enum State {
    NoState,
    Stopped,
    Running,
    Exited
  };
  State m_state;

  // queue of thread ids from the last ListThreads command
  // one gets dequeued for each ListFrames command until empty
  QQueue<QByteArray> m_pendingThreads;

  QTime m_elapsed;

  bool m_hasError;
  quint64 m_samples;

  int m_timeout;
};

}

#endif // RUNNER_H
