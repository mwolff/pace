/*
 * This file is part of pace
 *
 * Copyright 2011 Milian Wolff <mail@milianw.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef MODELBASE_H
#define MODELBASE_H
#include <QAbstractItemModel>
#include <QVector>
#include "parser/data.h"

namespace Pace {

class DataModel : public QAbstractItemModel
{
  Q_OBJECT

public:
  enum Type {
    TopDownTree,
    BottomUpTree,
    FlatList
  };
  explicit DataModel(Type type, QObject* parent = 0);
  virtual ~DataModel();

  virtual int columnCount(const QModelIndex& parent = QModelIndex()) const;
  virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;
  virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
  virtual QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const;
  virtual QModelIndex parent(const QModelIndex& child) const;

  virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

  void setSource(Data* data);
  void clear();

  int totalSamples();

  enum Columns {
    FunctionColumn,
    SelfSamplesColumn,
    SelfSamplesRelativeColumn,
    InclusiveSamplesColumn,
    InclusiveSamplesRelativeColumn,
    COLUMN_COUNT
  };

protected:
  Data* m_data;
  const Type m_type;

  struct Item {
    Data::Frame frame;
    int inclusiveSamples;
    int selfSamples;
    Item* parent;
    QVector<Item*> children;
  };
  QVector<Item*> m_items;
  QVector<Item*> m_topItems;
  int m_totalSnapshots;
};

}

#endif // MODELBASE_H
