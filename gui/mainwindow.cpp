/*
 * This file is part of pace
 *
 * Copyright 2011 Milian Wolff <mail@milianw.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "mainwindow.h"

#include <KFileDialog>
#include <KLocalizedString>
#include <KFilterDev>
#include <KStandardAction>
#include <KAction>
#include <KActionCollection>
#include <KRecentFilesAction>
#include <KMessageBox>
#include <KDebug>
#include <KStatusBar>

#include <QApplication>
#include <QSortFilterProxyModel>

#include "ui_mainwindow.h"

#include "parser/data.h"
#include "parser/parser.h"
#include "datamodel.h"

using namespace Pace;

void setupModel(DataModel* model, QTreeView* view,
                KFilterProxySearchLine* search)
{
  QSortFilterProxyModel *proxy = new QSortFilterProxyModel(view);
  proxy->setDynamicSortFilter(true);
  proxy->setSourceModel(model);
  view->setModel(proxy);
  search->setProxy(proxy);

  ///TODO: settings
  view->setSortingEnabled(true);
  view->sortByColumn(DataModel::SelfSamplesColumn);
  view->header()->setResizeMode(QHeaderView::Fixed);
  view->header()->setResizeMode(DataModel::FunctionColumn, QHeaderView::Stretch);
  view->hideColumn(DataModel::SelfSamplesRelativeColumn);
  view->hideColumn(DataModel::InclusiveSamplesRelativeColumn);
  view->resizeColumnToContents(DataModel::SelfSamplesColumn);
  view->resizeColumnToContents(DataModel::InclusiveSamplesColumn);
}

MainWindow::MainWindow(QWidget* parent, Qt::WindowFlags f)
: KParts::MainWindow(parent, f)
, m_ui(new Ui::MainWindow)
, m_close(0)
, m_recentFiles(0)
, m_currentFile(0)
, m_data(0)
, m_topDownModel(new DataModel(DataModel::TopDownTree, this))
, m_bottomUpModel(new DataModel(DataModel::BottomUpTree, this))
, m_flatListModel(new DataModel(DataModel::FlatList, this))
{
  m_ui->setupUi(this);

  setWindowTitle(i18n("Pace: GDB-based Sampling Profiler"));

  setupActions();
  setupGUI(StandardWindowOptions(Default ^ StatusBar));
  statusBar()->hide();

  setupModel(m_topDownModel, m_ui->topDownView, m_ui->topDownSearch);
  setupModel(m_bottomUpModel, m_ui->bottomUpView, m_ui->bottomUpSearch);
  setupModel(m_flatListModel, m_ui->hotspotsView, m_ui->hotspotsSearch);

  // open page
  m_ui->stackedWidget->setCurrentWidget(m_ui->openPage);
}

MainWindow::~MainWindow()
{
  delete m_ui;
  closeFile();
  m_recentFiles->saveEntries(KGlobal::config()->group( QString() ));
}

void MainWindow::setupActions()
{
  KAction* openFile = KStandardAction::open(this, SLOT(openFile()), actionCollection());
  KAction* reload = KStandardAction::redisplay(this, SLOT(reload()), actionCollection());
  actionCollection()->addAction("file_reload", reload);
  m_recentFiles = KStandardAction::openRecent(this, SLOT(openFile(KUrl)), actionCollection());
  m_recentFiles->loadEntries(KGlobal::config()->group( QString() ));

  m_close = KStandardAction::close(this, SLOT(closeFile()), actionCollection());
  m_close->setEnabled(false);

  KStandardAction::quit(qApp, SLOT(closeAllWindows()), actionCollection());

//   KStandardAction::preferences(this, SLOT(preferences()), actionCollection());

  //open page actions
  m_ui->openFile->setDefaultAction(openFile);
  m_ui->openFile->setText(i18n("Open Pace Data File"));
  m_ui->openFile->setIconSize(QSize(48, 48));
  m_ui->openFile->setIcon(KIcon("document-open"));
}

void MainWindow::openFile()
{
  QString file = KFileDialog::getOpenFileName(KUrl("kfiledialog:///pace"),
                                              QString("application/x-kde-pace"),
                                              this, i18n("Open Pace Output File"));
  if (!file.isEmpty()) {
    openFile(KUrl(file));
  }
}

void MainWindow::reload()
{
  if (m_currentFile.isValid()) {
    // copy to prevent madness
    openFile(KUrl(m_currentFile));
  }
}

void MainWindow::openFile(const KUrl& file)
{
  Q_ASSERT(file.isValid());
  QScopedPointer<QIODevice> device(KFilterDev::deviceForFile(file.toLocalFile()));

  if (!device->open(QIODevice::ReadOnly)) {
    KMessageBox::error(this, i18n("Could not open file <i>%1</i> for reading.", file.toLocalFile()), i18n("Could Not Read File"));
    return;
  }

  setUpdatesEnabled(false);

  if (m_data) {
    closeFile();
  }

  Parser p;
  m_data = p.parse(device.data());
  if (!m_data) {
    KMessageBox::error(this, i18n("Could not parse file <i>%1</i>.<br>"
                                  "Parse error in line %2:<br>%3", file.toLocalFile(), p.errorLine() + 1, p.errorLineString()),
                        i18n("Could Not Parse File"));
    setUpdatesEnabled(true);
    return;
  } else if (m_data->samples().isEmpty()) {
    KMessageBox::error(this, i18n("Empty data file <i>%1</i>.", file.toLocalFile()),
                        i18n("Empty Data File"));
    delete m_data;
    m_data = 0;
    setUpdatesEnabled(true);
    return;
  }

  m_currentFile = file;
  m_close->setEnabled(true);

  qDebug() << "loaded data file, samples:" << m_data->samples().size();
  m_topDownModel->setSource(m_data);
  m_bottomUpModel->setSource(m_data);
  m_flatListModel->setSource(m_data);

  ///TODO: cwd?
  m_ui->command->setText(m_data->cmdLine());
  m_ui->pid->setText(QString::number(m_data->pid()));
  m_ui->samples->setText(QString::number(m_data->samples().size()));
  m_ui->interval->setText(QString("%1 ms").arg(m_data->samplingInterval()));
  m_ui->stackedWidget->setCurrentWidget(m_ui->displayPage);

  setWindowTitle(i18n("Pace - evaluation of %1 (%2)", m_data->cmdLine(), file.fileName()));
  m_recentFiles->addUrl(file);

  setUpdatesEnabled(true);
}

void MainWindow::closeFile()
{
  if (!m_data) {
    return;
  }

  m_close->setEnabled(false);
  m_ui->stackedWidget->setCurrentWidget(m_ui->openPage);

  kDebug() << "closing file";

  delete m_data;
  m_data = 0;
  m_currentFile.clear();

  setWindowTitle(i18n("Pace: GDB-based Sampling Profiler"));
}


#include "mainwindow.moc"
