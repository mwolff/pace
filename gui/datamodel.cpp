/*
 * This file is part of pace
 *
 * Copyright 2011 Milian Wolff <mail@milianw.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "datamodel.h"

#include "parser/data.h"
#include <QQueue>
#include <KLocalizedString>
#include <QDebug>
#include <KGlobal>
#include <KLocale>
#include <KColorScheme>

using namespace Pace;

DataModel::DataModel(Type type, QObject* parent)
: QAbstractItemModel(parent)
, m_data(0)
, m_type(type)
, m_totalSnapshots(0)
{
  switch(m_type) {
    case FlatList:
      setObjectName("FlatList");
      break;
    case TopDownTree:
      setObjectName("TopDownTree");
      break;
    case BottomUpTree:
      setObjectName("BottomUpTree");
      break;
  }
}

DataModel::~DataModel()
{
  clear();
}

int DataModel::columnCount(const QModelIndex& /*parent*/) const
{
  return COLUMN_COUNT;
}

int DataModel::rowCount(const QModelIndex& parent) const
{
  if (!m_data || (parent.isValid() && parent.column() != 0)) {
    return 0;
  }

  if (!parent.isValid()) {
    // top level
    return m_topItems.count();
  }

  // somewhere in the three
  Q_ASSERT(parent.internalPointer());
  Item* parentItem = static_cast<Item*>(parent.internalPointer());
  Q_ASSERT(m_items.contains(parentItem));
  return parentItem->children.size();
}

QModelIndex DataModel::index(int row, int column, const QModelIndex& parent) const
{
  if (row < 0 || row >= rowCount(parent) || column < 0 || column >= columnCount(parent)) {
    // invalid
    return QModelIndex();
  }

  if (!parent.isValid()) {
    // top item
    return createIndex(row, column, static_cast<void*>(m_topItems.at(row)));
  }

  // somewhere in the tree
  if (parent.column() != 0) {
    // only first column has children
    return QModelIndex();
  }

  Q_ASSERT(parent.internalPointer());
  Item* parentItem = static_cast<Item*>(parent.internalPointer());
  Q_ASSERT(m_items.contains(parentItem));
  Item* childItem = parentItem->children.at(row);
  return createIndex(row, column, static_cast<void*>(childItem));
}

QModelIndex DataModel::parent(const QModelIndex& child) const
{
  if (!child.internalPointer()) {
    return QModelIndex();
  }

  Item* item = static_cast<Item*>(child.internalPointer());
  Q_ASSERT(m_items.contains(item));
  if (!item->parent) {
    // no parent
    return QModelIndex();
  }

  int parentRow = 0;
  if (item->parent->parent) {
    // somewhere in the tree
    parentRow = item->parent->parent->children.indexOf(item->parent);
  } else {
    // top item
    parentRow = m_topItems.indexOf(item->parent);
  }
  Q_ASSERT(parentRow != -1);

  return createIndex(parentRow, 0, static_cast<void*>(item->parent));
}

QVariant DataModel::data(const QModelIndex& index, int role) const
{
  if (role != Qt::DisplayRole && role != Qt::ToolTipRole && role != Qt::ForegroundRole) {
    return QVariant();
  }

  Q_ASSERT(index.row() >= 0 && index.row() < rowCount(index.parent()));
  Q_ASSERT(index.column() >= 0 && index.column() < columnCount(index.parent()));
  Q_ASSERT(m_data);
  Q_ASSERT(index.internalPointer());
  Item* item = static_cast<Item*>(index.internalPointer());
  Q_ASSERT(m_items.contains(item));

  if (role == Qt::ForegroundRole) {
    if (item->frame.compressedFunction == -1) {
      return KColorScheme(QPalette::Active).foreground(KColorScheme::InactiveText);
    }
    return QVariant();
  }

  if (role == Qt::ToolTipRole) {
    const QString function = m_data->uncompress(item->frame.compressedFunction, Pace::Data::Function);
    const QString file = m_data->uncompress(item->frame.compressedFile, Pace::Data::File);
    QString tooltip = "<qt><pre>";
    if (item->frame.line == -1) {
      tooltip += i18nc("tooltip: <function> in <file>",
                       "<b>%1</b>\nin %2", function, file);
    } else {
      tooltip += i18nc("tooltip: <function> in <file>:<line>",
                       "<b>%1</b>\nin %2:%3", function, file, item->frame.line);
    }
    tooltip += "</pre></qt>";
    return tooltip;
  }

  if (role == Qt::DisplayRole) {
    if (index.column() == FunctionColumn) {
      if (item->frame.compressedFunction == -1 && item->frame.compressedFile != -1) {
        return i18nc("unknown function in <file>", "?? in %1", m_data->uncompress(item->frame.compressedFile, Data::File));
      }
      return m_data->uncompress(item->frame.compressedFunction, Data::Function);
    } else if (index.column() == SelfSamplesColumn) {
      return item->selfSamples;
    } else if (index.column() == InclusiveSamplesColumn) {
      return item->inclusiveSamples;
    } else if (index.column() == SelfSamplesRelativeColumn) {
      return static_cast<QString>(
        KGlobal::locale()->formatNumber(qRound(double(item->selfSamples) / m_totalSnapshots * 100), 2) + '%'
      );
    } else if (index.column() == InclusiveSamplesRelativeColumn) {
      return static_cast<QString>(
        KGlobal::locale()->formatNumber(qRound(double(item->inclusiveSamples) / m_totalSnapshots * 100), 2) + '%'
      );
    }
  }

  Q_ASSERT(false);
  return QVariant();
}

void DataModel::clear()
{
  beginResetModel();
  qDeleteAll(m_items);
  m_items.clear();
  m_topItems.clear();
  m_data = 0;
  m_totalSnapshots = 0;
  endResetModel();
}


QVariant DataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role != Qt::DisplayRole || orientation != Qt::Horizontal) {
    return QAbstractItemModel::headerData(section, orientation, role);
  }

  if (section == FunctionColumn) {
    return i18n("Function");
  } else if (section == SelfSamplesColumn) {
    return i18n("Self Samples");
  } else if (section == InclusiveSamplesColumn) {
    return i18nc("Inclusive Samples", "Incl. Samples");
  } else if (section == SelfSamplesRelativeColumn) {
    return i18nc("Relative Self Samples", "Rel. Self Samples");
  } else if (section == InclusiveSamplesRelativeColumn) {
    return i18nc("Relative Inclusive Samples", "Rel. Incl. Samples");
  }

  Q_ASSERT(false);
  return QVariant();
}

void DataModel::setSource(Data* data)
{
  clear();

  beginResetModel();

  m_data = data;

  QHash<int, Item*> knownItems;

  foreach(const Data::Sample& sample, data->samples()) {
    ///TODO: separate by thread?
    foreach(const Data::Backtrace& trace, sample) {
      if (trace.isEmpty()) {
        continue;
      }

      Item* parent = 0;
      int firstFrame = -1;
      for(int i = 0; i < trace.size(); ++i) {
        if (trace.at(i).compressedFunction != -1) {
          firstFrame = i;
          break;
        }
      }
      if (firstFrame == -1) {
        // no debug symbols in this trace
        continue;
      }
      const int increment = m_type == TopDownTree ? -1 : +1;
      const int start = m_type == TopDownTree ? trace.count() - 1 : firstFrame;
      const int end = m_type == TopDownTree ? -1 : trace.count();
      for(int i = start; i != end; i += increment) {
        const Data::Frame& frame = trace.at(i);
        if (!parent && frame.compressedFunction == -1) {
          // skip leading functions without debug symbols
          continue;
        } else if (frame.compressedFunction == -1
                    && frame.line == parent->frame.line
                    && frame.compressedFile == parent->frame.compressedFile)
        {
          // merge "equal" frames
          continue;
        }
        // TODO: improve performance? i.e. better than linear here?
        //       esp. for the flat list this is really required!
        QVector<Item*>& target = parent ? parent->children : m_topItems;
        Item* item = 0;
        foreach(Item* sibling, target) {
          if (sibling->frame.compressedFunction == frame.compressedFunction
              && sibling->frame.compressedFile == frame.compressedFile)
          {
            item = sibling;
            break;
          }
        }
        if (!item) {
          item = new Item;
          m_items << item;
          if (!parent) {
            m_topItems << item;
          } else {
            parent->children << item;
          }
          item->frame = frame;
          Q_ASSERT(!m_data->uncompress(item->frame.compressedAddress, Data::Address).isEmpty());
          item->selfSamples = 0;
          item->inclusiveSamples = 0;
          item->parent = parent;
        }
        item->inclusiveSamples++;
        if (i == firstFrame) {
          item->selfSamples++;
        }
        if (m_type != FlatList) {
          parent = item;
        }
      }
      if (parent || m_type == FlatList) {
        ++m_totalSnapshots;
      }
    }
  }

  endResetModel();
}

int DataModel::totalSamples()
{
  return m_totalSnapshots;
}

#include "datamodel.moc"
