/*
 * This file is part of pace
 *
 * Copyright 2011 Milian Wolff <mail@milianw.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "parser.h"

#include "data.h"

#include <QDebug>
#include <QStringList>

#define IF_DEBUG(x)

using namespace Pace;

Parser::Parser()
: m_errorLine(-1)
{

}

Parser::~Parser()
{

}

Data* Parser::parse(QIODevice* file)
{
  Q_ASSERT(file->isOpen());
  Q_ASSERT(file->isReadable());

  Data* data = new Data;

  Data::Sample* currentSample = 0;
  Data::Backtrace* currentBacktrace = 0;
  int lineNr = -1;

  QHash<QString, int> addressMap;
  QHash<QString, int> functionMap;
  QHash<QString, int> locationMap;

  while(!file->atEnd()) {
    ++lineNr;
    const QByteArray line = file->readLine();
    bool ok = true;

    if (lineNr == 0 && line == "paceprofile\n") {
      continue;
    } else if (lineNr == 1 && data->m_interval == -1 && line.startsWith("interval: ")) {
      data->m_interval = line.mid(10, line.length() - 10  - 1).toInt(&ok);
      IF_DEBUG(qDebug() << "interval:" << data->m_interval;)
    } else if (lineNr == 2 && data->m_pid == -1 && line.startsWith("~\"process ")) {
      data->m_pid = line.mid(10, line.length() - 10 - 4).toInt(&ok);
      IF_DEBUG(qDebug() << "pid:" << data->m_pid << line.mid(10, line.length() - 10 - 4);)
    } else if (lineNr == 3 && line.startsWith("~\"cmdline = '")) {
      data->m_cmdLine = line.mid(13, line.length() - 13 - 5);
      IF_DEBUG(qDebug() << "cmdline:" << data->m_cmdLine;)
    } else if (lineNr == 4 && line.startsWith("~\"cwd = '")) {
      data->m_cwd = line.mid(9, line.length() - 9 - 5);
      IF_DEBUG(qDebug() << "cwd:" << data->m_cwd;)
    } else if (lineNr == 5 && line.startsWith("~\"exe = '")) {
      data->m_exe = line.mid(9, line.length() - 9 - 5);
      IF_DEBUG(qDebug() << "exe:" << data->m_exe;)
    } else if (line == "sample:\n") {
      IF_DEBUG(qDebug() << "new sample";)
      ///TODO: reserve-optimization?
      Data::Sample sample;
      data->m_samples.append(sample);
      currentSample = &data->m_samples.last();
      currentBacktrace = 0;
    } else if (line.startsWith("~\"\\nThread")) {
      IF_DEBUG(qDebug() << "new thread backtrace";)
      ///TODO: save thread info
      ///TODO: reserve-optimization?
      Q_ASSERT(currentSample);
      Data::Backtrace trace;
      currentSample->append(trace);
      currentBacktrace = &currentSample->last();
    } else if (line.startsWith("~\"#")) {
      Q_ASSERT(currentBacktrace);
      Data::Frame f;
      QList<QByteArray> contents = line.split(' ');
      QByteArray ignore;
      IF_DEBUG(qDebug() << "new frame:" << contents);
      // ignore level
      ignore = contents.takeFirst();
      Q_ASSERT(ignore.startsWith("~\"#"));
      // ignore empty
      while(contents.first().isEmpty()) {
        contents.takeFirst();
      }
      // address (if set - seems like this may happen...)
      if (contents.first().startsWith("0x")) {
        const QString address = contents.takeFirst();
        Q_ASSERT(address.startsWith("0x"));
        f.compressedAddress = data->compress(address, Data::Address, &addressMap);
        IF_DEBUG(qDebug() << "address:" << address << f.compressedAddress;)
        // "in"
        ignore = contents.takeFirst();
        Q_ASSERT(ignore == "in");
      }
      // now take location from end of contents
      // but gracefully handle frames without enough debug symbols (end on closing-paren)
      if (!contents.last().endsWith(")\\n\"\n")) {
        QString location = contents.takeLast();
        Q_ASSERT(location.endsWith("\\n\"\n"));
        location.chop(4);
        // ignore from/at
        ignore = contents.takeLast();
        Q_ASSERT(ignore == "from" || ignore == "at");
        if (ignore == "at") {
          // we have line info
          int pos = location.lastIndexOf(':');
          ok = pos != -1;
          if (pos != -1) {
            f.line = location.right(location.length() - pos - 1).toInt(&ok);
            location.chop(location.length() - pos);
          } else {
            ok = false;
          }
        }
        f.compressedFile = data->compress(location, Data::File, &locationMap);
        IF_DEBUG(qDebug() << "location:" << location << f.compressedFile << f.line;)
      } else {
        // actually part of the function, strip trailing junk and re-add it
        QByteArray last = contents.takeLast();
        last.chop(4);
        contents.append(last);
      }
      // the rest is our function
      QString function = contents.takeFirst();
      if (function == "??") {
        Q_ASSERT(contents.size() == 1);
        Q_ASSERT(contents.last() == "()");
        f.compressedFunction = -1;
      } else {
        ///TODO: setting for removal of argument-values
        bool inArgList = false;
        foreach(const QByteArray& p, contents) {
          QString part = QString::fromUtf8(p);
          if (!inArgList && part.startsWith('(')) {
            inArgList = true;
            if (part.startsWith("(this=")) {
              // remove 'this' argument
              part.remove(QRegExp("this=0x[a-f0-9]+,?"));
            }
          }
          if (inArgList && part.contains('=')) {
            static QRegExp pattern("=[^ ,\\)]+");
            part.remove(pattern);
          }
          function += ' ' + part;
        }
        f.compressedFunction = data->compress(function, Data::Function, &functionMap);
      }
      IF_DEBUG(qDebug() << "function:" << function << f.compressedFunction);
      currentBacktrace->append(f);
    } else {
      // invalid/unexpected line
      ok = false;
    }

    if (!ok) {
      qWarning() << "parse error in line" << lineNr << ":" << endl << line;
      delete data;
      m_errorLine = lineNr;
      m_errorLineString = line;
      Q_ASSERT(false);
      return 0;
    }
    /*
    } else if (type == "frame:") {
      Q_ASSERT(pendingFrames);
      Q_ASSERT(currentBacktrace);
      --pendingFrames;
      int level;
      in >> level;
      Data::Frame f;
      in >> f.compressedAddress;
      in >> f.compressedFunction;
      in >> f.compressedFile;
      in >> f.line;
      currentBacktrace->append(f);
    } else if (type == "func:" || type == "addr:" || type == "file:") {
      int index;
      in >> index;
      QString string = in.readLine();
      if (index >= 0 && !string.isEmpty()) {
        Data::CompressionType compressionType;
        if (type == "func:") {
          compressionType = Data::Function;
        } else if (type == "addr:") {
          compressionType = Data::Address;
        } else if (type == "file:") {
          compressionType = Data::File;
        }
        data->addCompressed(index, string, compressionType);
      } else {
        qWarning() << "invalid compression line: " << line;
      }
    } else {
    }
    */
  }

  return data;
}

int Parser::errorLine() const
{
  return m_errorLine;
}

QString Parser::errorLineString() const
{
  return m_errorLineString;
}
