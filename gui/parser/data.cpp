/*
 * This file is part of pace
 *
 * Copyright 2011 Milian Wolff <mail@milianw.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "data.h"

using namespace Pace;

Data::Data()
: m_pid(-1)
, m_interval(-1)
{

}

Data::~Data()
{

}

const QVector<Data::Sample>& Data::samples() const
{
  return m_samples;
}

QString Data::cmdLine() const
{
  return m_cmdLine;
}

QString Data::cwd() const
{
  return m_cwd;
}

QString Data::exe() const
{
  return m_exe;
}

int Data::pid() const
{
  return m_pid;
}

int Data::samplingInterval() const
{
  return m_interval;
}

QString Data::uncompress(const int input, Data::CompressionType type) const
{
  if (input == -1) {
    return QLatin1String("??");
  }

  const QHash<int, QString>* map = 0;
  switch(type) {
    case Address:
      map = &m_addresses;
      break;
    case Function:
      map = &m_functions;
      break;
    case File:
      map = &m_files;
      break;
  }

  Q_ASSERT(map->contains(input));
  return map->value(input);
}

inline QString unescape(const QString& message)
{
  if (!message.contains('\\')) {
    return message;
  }

  int length = message.length();
  QString message2;
  message2.reserve(length);
  // The [1,length-1] range removes quotes without extra
  // call to 'mid'
  int target_index = 0;
  for(int i = 1, e = length-1; i != e; ++i)
  {
      int translated = -1;
      if (message[i] == '\\')
      {
          if (i+1 < length)
          {
              // TODO: implement all the other escapes, maybe
              if (message[i+1] == 'n')
              {
                  translated = '\n';
              }
              else if (message[i+1] == '\\')
              {
                  translated = '\\';
              }
              else if (message[i+1] == '"')
              {
                  translated = '"';
              }
              else if (message[i+1] == 't')
              {
                  translated = '"';
              }
              else if (message[i+1] == 't')
              {
                  translated = '\t';
              }
          }
      }

      if (translated != -1)
      {
          message2[target_index++] = translated;
          ++i;
      }
      else
      {
          message2[target_index++] = message[i];
      }
  }
  message2.squeeze();
  return message2;
}

int Data::compress(const QString& value, Data::CompressionType type, QHash< QString, int >* map)
{
  if (value.isEmpty() || value == "??") {
    return -1;
  }

  const QHash< QString, int >::const_iterator it = map->constFind(value);
  if (it == map->constEnd()) {
    int key = map->size();
    map->insert(value, key);
    addCompressed(key, value, type);
    return key;
  } else {
    return it.value();
  }
}

void Data::addCompressed(const int key, const QString& value, Data::CompressionType type)
{
  Q_ASSERT(key >= 0);

  QHash<int, QString>* map = 0;
  switch(type) {
    case Address:
      map = &m_addresses;
      break;
    case Function:
      map = &m_functions;
      break;
    case File:
      map = &m_files;
      break;
  }

  Q_ASSERT(!map->contains(key));
  map->insert(key, unescape(value));
}
