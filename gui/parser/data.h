/*
 * This file is part of pace
 *
 * Copyright 2011 Milian Wolff <mail@milianw.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef DATA_H
#define DATA_H

#include <QVector>
#include <QString>
#include <QHash>

#include "parser.h"

namespace Pace {

class Sample;

class Data {
public:
  Data();
  ~Data();

  struct Frame
  {
    Frame()
    : compressedAddress(-1)
    , compressedFunction(-1)
    , compressedFile(-1)
    , line(-1)
    {
    }
    int compressedAddress;
    int compressedFunction;
    int compressedFile;
    int line;
  };
  typedef QVector<Frame> Backtrace;
  // one backtrace per thread
  typedef QVector<Backtrace> Sample;

  const QVector<Sample>& samples() const;

  // commandline of the profiled app
  QString cmdLine() const;
  // current working directory of profiled app
  // only read once after attaching to the app
  QString cwd() const;
  // executable of profiled app
  QString exe() const;
  // pid of the profiled app
  int pid() const;
  // sampling interval of the profile
  int samplingInterval() const;

  enum CompressionType {
    Address,
    Function,
    File
  };
  QString uncompress(const int input, Data::CompressionType type) const;

private:
  friend class Parser;

  int compress(const QString& value, Data::CompressionType type, QHash<QString, int>* map);
  void addCompressed(const int key, const QString& data, Data::CompressionType type);

  QString m_cmdLine;
  QString m_cwd;
  QString m_exe;
  int m_pid;
  int m_interval;
  QVector<Sample> m_samples;
  QHash<int, QString> m_addresses;
  QHash<int, QString> m_functions;
  QHash<int, QString> m_files;
};

inline bool operator==(const Data::Frame& l, const Data::Frame& r)
{
  //TODO: investigate, actually only adress comparison should suffice
  //      I think since that should be unique for frames
  return l.compressedAddress == r.compressedAddress &&
         l.compressedFunction == r.compressedFunction &&
         l.compressedFile == r.compressedFile &&
         l.line == r.line;
}

}

#endif // DATA_H
