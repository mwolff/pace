/*
 * This file is part of pace
 *
 * Copyright 2011 Milian Wolff <mail@milianw.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <KParts/MainWindow>

class KRecentFilesAction;
namespace Ui {
class MainWindow;
}

class KAction;

namespace Pace {

class Data;
class DataModel;

class MainWindow : public KParts::MainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget* parent = 0, Qt::WindowFlags f = 0);
  virtual ~MainWindow();

  void setupActions();

public slots:
  /**
   * Open a dialog to pick a massif output file to display.
   */
  void openFile();

  /**
   * Opens @p file as massif output file and visualize it.
   */
  void openFile(const KUrl& file);

  /**
   * reload currently opened file
   */
  void reload();

  /**
   * Close currently opened file.
   */
  void closeFile();

private:
  Ui::MainWindow* m_ui;

  KAction* m_close;
  KRecentFilesAction* m_recentFiles;

  KUrl m_currentFile;

  Data* m_data;

  DataModel* m_topDownModel;
  DataModel* m_bottomUpModel;
  DataModel* m_flatListModel;
};

}

#endif // MAINWINDOW_H
