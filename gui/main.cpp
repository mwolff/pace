/*
 * This file is part of pace
 *
 * Copyright 2011 Milian Wolff <mail@milianw.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <KLocalizedString>
#include <KAboutData>
#include <KApplication>
#include <KCmdLineArgs>
#include <KCmdLineOptions>

#include <QDebug>

#include "mainwindow.h"

int main(int argc, char **argv) {
  KAboutData aboutData( "pace", 0, ki18n( "Pace" ),
                        "0.1", ki18n("Visualization GUI for Pace, a sampling based application profiler based on GDB."),
                        KAboutData::License_LGPL,
                        ki18n( "Copyright 2011, Milian Wolff <mail@milianw.de>" ),
                        KLocalizedString(), "", "mail@milianw.de" );
  ///FIXME: get a proper icon
  aboutData.setProgramIconName("preferences-desktop-launch-feedback");

  KCmdLineArgs::init( argc, argv, &aboutData, KCmdLineArgs::CmdLineArgNone );
  KCmdLineOptions options;
  options.add("+file", ki18n("Open pace.out file and visualize it."));

  KCmdLineArgs::addCmdLineOptions( options );
  KCmdLineArgs* args = KCmdLineArgs::parsedArgs();

  KApplication app;

  for (int i = 0; i < qMax(1, args->count()); ++i) {
    Pace::MainWindow* window = new Pace::MainWindow;
    if (args->count()) {
      window->openFile(args->url(i));
    }
    window->show();
  }

  return app.exec();
}