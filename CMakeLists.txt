project(pace)

cmake_minimum_required(VERSION 2.8)

# optionally find KDE4 for GUI
find_package(KDE4)
if(KDE4_FOUND)
  include (KDE4Defaults)
  include_directories( ${KDE4_INCLUDES} )
  add_definitions(-DHAVE_KDE)
else(KDE4_FOUND)
  message(STATUS "KDE4 not found, cannot build pace, the GUI to visualize pace-monitor profiles.")

  find_package(Qt4 REQUIRED)
  include_directories( ${QT_INCLUDES} )
endif(KDE4_FOUND)

# generic definitions
add_definitions(
  # fast concatenation
  -DQT_USE_FAST_CONCATENATION -DQT_USE_FAST_OPERATOR_PLUS
  # strict iterators
  -DQT_STRICT_ITERATORS
)

# pace-monitor
add_subdirectory(runner)

# pace: only when kde is available
if(KDE4_FOUND)
  add_subdirectory(gui)
endif(KDE4_FOUND)